package com.source.roma.exception;

public class NotFoundException extends GlobalException{
    public NotFoundException(String message, int code)
    {
        super(message, code);
    }
}
