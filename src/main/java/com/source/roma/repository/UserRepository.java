package com.source.roma.repository;

import com.source.roma.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;



public interface UserRepository extends JpaRepository<User, Integer> {
    public User findByUsername(String username);
}


