package com.source.roma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RomaApplication {

	public static void main(String[] args) {
		SpringApplication.run(RomaApplication.class, args);
	}

}
