package com.source.roma.api;

import com.source.roma.domain.User;
import com.source.roma.exception.NotFoundException;
import com.source.roma.service.UserService;
import com.source.roma.util.Message;
import com.source.roma.util.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/user")
@CrossOrigin
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping(value = "/login")
    public Message loginUser(@RequestBody User user)
    {
        Token t = Token.getToken();
        User u = (User) userService.getUser(user.getUsername());
        if(u == null){
            return new Message(Message.TYPE.ERROR, "INFO", "用户未找到");
        }
        if(u.getPassword().equals(user.getPassword()))
            return new Message(Message.TYPE.OK, "ok", t.addUser(u));
        else
            return new Message(Message.TYPE.AUTH_FAILED, "INFO", "密码错误");
    }
    @GetMapping(value = "/logout")
    public Message logoutUser(HttpServletRequest request){
        Token t = Token.getToken();
        t.removeToken(request.getHeader("Authorization"));
        return new Message(Message.TYPE.OK, "ok", "");
    }
}
