package com.source.roma.domain;
import com.sun.istack.NotNull;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name="room")
public class Room {
    @Id @Column(name="r_id") @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name="r_name")
    private String name;
    @Column(name="r_location")
    private String location;
    @Column(name="r_description")
    private String description;
    @Column(name="r_seat_map")
    private Seat seats;


}
