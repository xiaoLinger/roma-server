package com.source.roma;

import com.source.roma.dao.User;
import com.source.roma.dao.UserDaoImpl;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RomaApplicationTests {

	@Test
	void contextLoads() {
		User user = new User("Hello", "Password", "Desc");
		UserDaoImpl userDao = new UserDaoImpl();
		userDao.insertUser(user);
	}

}
